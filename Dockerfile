FROM openjdk:12
ADD target/docker-spring-boot-berte_jeferson.jar docker-spring-boot-berte_jeferson.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-berte_jeferson.jar"]
